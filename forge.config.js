module.exports = {
  packagerConfig: {
    // node_modules might need to be added back here?
    ignore: "(.git|.vscode|docs|.gitignore|README.md|LICENSE.md)",
    icon: "./src/icons/fsl-desktop",
    osxSign: {
      // notes: 
      // 1. login to the apple developer website
      // 2. create a new certificate for apple development (following all instructions)
      // 3. download the certificate and double click to install
      // 4. make sure you have the latest "Apple Worldwide Developer Relations Certification Authority" certificate installed
      // 5. sign out of xcode developer accounts
      // 6. log out and back i 7. pray that this process worked
      // identity: "Apple Development: Taylor Hanayik (TYF3AXHPA8)"
      // identity: "Developer ID Application: University of Oxford (XXXXX)"
      // find identities installed: security find-identity -p codesigning -v 
      // codesign manually: codesign -s "Apple Development: Taylor Hanayik (TYF3AXHPA8)" -vvv out/make/zip/darwin/arm64/fsl-desktop.app
      // check code signing worked: codesign -dv --verbose=4 out/make/zip/darwin/arm64/fsl-desktop.app
    }, // object must exist even if empty
    // osxNotarize: {
    //   // can only notarise if using a "Developer ID” type of certificate during code signing
    //   tool: "notarytool",
    //   appleId: "email",
    //   appleIdPassword: "password",
    //   teamId: "teamID"
    // }
  },
  makers: [
    {
      name: '@electron-forge/maker-zip',
      platforms: ['darwin', 'linux'],
    },
  ],
};
