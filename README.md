# FSL Desktop

The FSL Desktop application is an Electron app that serves as the main entry point for FSL's graphical user interfaces. 

# Important notes
- The FSL desktop application does not contain a GUI of its own. It is simply a means of launching the web-based FSL GUIs that get installed with new FSL versions (yet to be released).
- The desktop application orchestrates many other background processes that are required for the FSL GUIs to function. These processes are started and stopped automatically when the desktop application is launched and closed, respectively.
- The desktop application interacts with the FSL React-based web GUIs via events defined in `src/events.js` and `src/preload.js`. These events are triggered by the web GUIs and handled by the desktop application. The desktop application also triggers events that are handled by the web GUIs via the Electron IPC mechanisms. 

# Development

## Prerequisites
- Node.js (preferably the latest LTS version)
- NPM
- FSL (preferably the latest version)
- other FSL GUI git repositories available on your system such as:
  - [bet](https://git.fmrib.ox.ac.uk/fsl/gui/bet)
  - [flirt](https://git.fmrib.ox.ac.uk/fsl/gui/flirt)
  - see full list [here](https://git.fmrib.ox.ac.uk/fsl/gui)

## Working in dev mode

The FSL GUIs are meant to be modular so they can be developed and distributed independently. However, this means that during *development* the desktop app needs to be able to find the web GUI repos on your system. This is accomplished via the `src/devPorts.js` file. These are hard coded values that match the server ports in each of the GUI repos.

To run the desktop app in dev mode, follow these steps:

1. `npm install`
2. `npm run dev`

**However**, that alone won't launch a GUI. You must also open a terminal to a React GUI repo and run `npm run dev` there as well. For example, if you want to launch the FSL BET GUI, you must also open a terminal to the `fsl/gui/bet` repo and run `npm run dev` there.

At this point, you should be able to launch the BET GUI from the FSL Desktop app's main menu.

## Building the desktop app

The desktop app is built and bundled for distribution using electron-forge. To build the app, run `npm run make`. This will create a distributable version of the app in the `out` directory.

To build for specific platforms you can use the individual scripts:
- `npm run makeMacArm64 # for Apple Silicon`
- `npm run makeMacX64 # for Intel Macs`
- `npm run makeLinuxX64 # for Linux non-ARM`
