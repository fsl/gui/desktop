/**
 * Object containing development ports for various tools.
 * @typedef {Object} DevPorts
 * @property {number} dashboard - Port for the dashboard GUI.
 * @property {number} bet - Port for the Brain Extraction Tool (BET).
 * @property {number} fast - Port for the FAST segmentation tool.
 * @property {number} flirt - Port for the FSL FLIRT registration tool.
 * @property {number} fslstats - Port for the fslstats statistics tool.
 * @property {number} glm - Port for the FSL GLM tool.
 * @property {number} feat - Port for the FSL FEAT tool.
 * @property {number} prepare_fieldmap - Port for the FSL prepare_fieldmap tool.
 * @property {number} fslmaths - Port for the FSL fslmaths tool.
 * @property {number} featquery - Port for the FSL featquery tool.
 * @property {number} melodic - Port for the FSL MELODIC tool.
 * @property {number} fdt - Port for the FSL FDT tool.
 * @property {number} viz - Port for the FSL VIZ tool.
 * @property {number} history - Port for the FSL history tool.
 */

/**
 * Development ports for various tools.
 * @type {DevPorts}
 */
const devPorts = {
    dashboard: 8888,
    bet: 5173,
    fast: 5174,
    flirt: 5175,
    fslstats: 5176,
    glm: 5177,
    feat: 5178,
    prepare_fieldmap: 5179,
    fslmaths: 5180,
    featquery: 5181,
    melodic: 5182,
    fdt: 5183,
    history: 5184,
    applyxfm: 5185,
    invertxfm: 5186,
    concatxfm: 5187,
};

module.exports.devPorts = devPorts