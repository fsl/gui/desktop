import React from 'react'
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';

function App() {

  const [guis, setGUIs] = React.useState([])
  const [filteredGuis, setFilteredGuis] = React.useState([])

  // when mounted, list the installed GUIs
  React.useEffect(() => {
    async function listGUIs() {
      const FSL = window.FSL
      const installedGuis = await FSL.listInstalledGuis()
      console.log(installedGuis)
      setGUIs(installedGuis)
    }
    listGUIs()
  }, [])

  // when guis changes (a reload or first load), set the filtered guis to all guis
  React.useEffect(() => {
    setFilteredGuis(guis)
  }, [guis])

  function onSearch(text){
    // filter the guis based on the search text
    // filter by name and description
    const filtered = guis.filter(gui => {
      return gui.name.toLowerCase().includes(text.toLowerCase())
    })
    setFilteredGuis(filtered)
  }

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        // justifyContent: 'center',
        height: '100vh',
        width: '100vw',
      }}
    >
      <CssBaseline /> 
      {/* text input box for search */}
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          width: '100%',
          padding: 2
        }}
      >
        <TextField
          sx={{width: '80%'}}
          id="outlined-basic"
          label="Search"
          variant="outlined"
          onChange={(e) => onSearch(e.target.value)}
          placeholder='Search for FSL applications by name (leave empty for all GUIs)'
        >

        </TextField>
      </Box>

      {/* box of gui cards */}
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          height: '100%',
          width: '100%',
        }}
      >

      {/* for each gui, make a paper card with a button to launch the GUI */}
      {filteredGuis.map((gui) => (
        <Paper
          key={gui.name}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            width: 300,
            height: 200,
            m: 2,
          }}
        >
          <Typography sx={{m: 1, textAlign: 'center'}} variant="h5" component="h2">
            {gui.name}
          </Typography>
          <Typography sx={{ m: 1.5, textAlign: 'center', wordBreak:'break-word'}} color="text.secondary">
            {gui.description}
          </Typography>
          <Button 
            variant="contained" 
            onClick={() => window.FSL.launchGui(gui.name.toLowerCase())}
            >
            Launch
          </Button>
        </Paper>
      ))}
      {/* end of row */}
      </Box> 
      {/* end of col */}
    </Box>
  )
}
  
export default App
