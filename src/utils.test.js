const fs = require('fs');
const path = require('path');
const utils = require('./utils');
const {defaultConfig} = require('./configDefaults');

test('creates a command string', () => {
    const commandObject = {
        command: 'bet',
        opts: {
            'in': 'test.nii.gz',
            'out': 'test_brain.nii.gz',
            '-m': true,
            '-f': 0.4
        },
    };
    const commandString = utils.createCommandString(commandObject);
    expect(commandString).toBe('bet test.nii.gz test_brain.nii.gz -m -f 0.4');
})

test('saves a command object as a json file', async () => {
    const commandObject = {
        command: 'bet',
        opts: {
            'in': 'test.nii.gz',
            'out': 'test_brain.nii.gz',
            '-m': true,
            '-f': 0.4
        },
    };
    const filePath = await utils.saveCommand(commandObject);
    // read the JSON file and compare the fields
    const data = fs.readFileSync(filePath);
    const json = JSON.parse(data);
    expect(json.command).toBe('bet');
    expect(json.opts.in).toBe('test.nii.gz');
    expect(json.opts.out).toBe('test_brain.nii.gz');
    expect(json.opts['-m']).toBe(true);
    expect(json.opts['-f']).toBe(0.4);
    // delete the file
    fs.unlinkSync(filePath);
})

test('reads the history directory', async () => {
    // create a temporary directory
    const tmpDir = fs.mkdtempSync('test');
    // create 5 temporary command objects and
    // save them using saveCommand
    for (let i = 0; i < 1; i++) {
        const commandObject = {
            command: 'bet',
            opts: {
                'in': `test_${i}.nii.gz`,
                'out': `test_${i}_brain.nii.gz`,
                '-m': true,
                '-f': 0.4
            },
        };
        let filepath = await utils.saveCommand(commandObject, tmpDir);
    }
    // pause for a second 
    await new Promise(resolve => setTimeout(resolve, 1000));
    // read the directory
    const history = await utils.readHistory(tmpDir);
    // expect the length to be 5
    expect(history.length).toBe(1);
    // expect the first command to be the most recent one saved
    expect(history[0].opts.in).toBe('test_0.nii.gz');
    // delete the directory
    fs.rmSync(tmpDir, { recursive: true });
})

test('lists installed guis async', async () => {
    // create a temporary directory
    const tmpDir = fs.mkdtempSync('test');
    // make some fake gui folders, each containing an index.html file
    const fakeGuis = ['bet', 'fast', 'flirt'];
    for (let i = 0; i < fakeGuis.length; i++) {
        const guiDir = path.join(tmpDir, fakeGuis[i]);
        fs.mkdirSync(guiDir, { recursive: true });
        fs.writeFileSync(path.join(guiDir, 'index.html'), 'test');
    }
    const installedGUIs = await utils.listGuis(tmpDir);
    expect(installedGUIs.length).toBe(3);
    // expect the gui names to be bet, fast, and flirt
    expect(installedGUIs[0].name).toBe('bet');
    expect(installedGUIs[1].name).toBe('fast');
    expect(installedGUIs[2].name).toBe('flirt');
    // delete the directory
    fs.rmSync(tmpDir, { recursive: true });
})

test('lists installed guis sync', () => {
    // create a temporary directory
    const tmpDir = fs.mkdtempSync('test');
    // make some fake gui folders, each containing an index.html file
    const fakeGuis = ['bet', 'fast', 'flirt'];
    for (let i = 0; i < fakeGuis.length; i++) {
        const guiDir = path.join(tmpDir, fakeGuis[i]);
        fs.mkdirSync(guiDir, { recursive: true });
        fs.writeFileSync(path.join(guiDir, 'index.html'), 'test');
    }
    const installedGUIs = utils.listGuisSync(tmpDir);
    expect(installedGUIs.length).toBe(3);
    // expect the gui names to be bet, fast, and flirt
    expect(installedGUIs[0].name).toBe('bet');
    expect(installedGUIs[1].name).toBe('fast');
    expect(installedGUIs[2].name).toBe('flirt');
    // delete the directory
    fs.rmSync(tmpDir, { recursive: true });
})

test('reads the default config file', () => {
    const config = utils.readGuiConfig();
    // expect each key in the config object to equal the default value
    expect(config).toEqual(defaultConfig);

})