const request = require("supertest")
const {app} = require("./fileServer")
const fs = require("fs")
const path = require("path")

test("GET /file", async () => {
    const response = await request(app).get("/file")
    expect(response.statusCode).toBe(200)
    expect(response.text).toBe("no filename given")
})

test("GET /file?filename=foo", async () => {
    const response = await request(app).get("/file?filename=foo")
    expect(response.statusCode).toBe(200)
    expect(response.text).toBe("file not found: foo")
})

test("GET /file?filename=package.json", async () => {
    // make temp file at the same path as this file
    const testFile = path.join(__dirname, 'index.js')
    const response = await request(app).get(`/file?filename=${testFile}`)
    expect(response.statusCode).toBe(200)
    // expect the response to be the contents of the file
    const data = fs.readFileSync(testFile, 'utf-8');
    expect(response.text).toBe(data)
})