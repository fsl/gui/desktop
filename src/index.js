const { app, BrowserWindow, Menu, ipcMain, dialog } = require('electron');
const path = require('path');
const os = require('os');
const fs = require('fs');
const { fork, execSync } = require("child_process");
const { devPorts } = require('./devPorts');
const { events } = require('./events');
const { listGuisSync } = require('./utils');
let argv = require('minimist')(process.argv.slice(2));

// ignore gpu blacklist
app.commandLine.appendSwitch('--ignore-gpu-blacklist');

// launch the fileServer as a background process
fileServer = fork(
  path.join(__dirname, "fileServer.js"),
  { env: { FORK: true, ...process.env } }
);

/**
 * handles setting the process env variables for the fileServer port and host
 * @param {number} port - the port the fileServer is listening on
 * @returns {undefined}
 * @function
 * @example
 * onFileServerPort(12345);
 * console.log(process.env.FSLGUI_FILESERVER_PORT);
 * // 12345
 * console.log(process.env.FSLGUI_HOST);
 * // localhost
 */
function onFileServerPort(port) {
  process.env.FSLGUI_FILESERVER_PORT = port;
  process.env.FSLGUI_HOST = 'localhost';
}

// handler function for the fileServer port message
function handleFileServerMessage(message) {
  // msg is expected to be a JSON object (automatically serialized and deserialized by process.send and 'message')
  // a message object has a 'type' and a 'value' as properties
  if (message.type === "fileServerPort") {
    onFileServerPort(message.value);
  }
}

// register the handler function for fileServer messages
fileServer.on("message", (message) => {
  handleFileServerMessage(message);
});


// function to run the env command on macOS and linux to get all FSL env variables
function getFslEnv() {
  // get the SHELL
  let shell = os.userInfo().shell || process.env.SHELL || '/bin/zsh';
  let shellName = path.basename(shell);
  let bourneShells = ['zsh', 'sh', 'bash', 'dash']
  let cShells = ['csh', 'tcsh']
  let shellProfiles = {
    zsh: ['.zprofile'],
    sh: ['.profile'],
    bash: ['.bash_profile', '.profile'],
    dash: ['.bash_profile', '.profile'],
    csh: ['.cshrc'],
    tcsh: ['.tcshrc']
  }

  let fslEnv = {};
  let profilePath = '';
  // if the shell is a bourne shell
  if (bourneShells.includes(shellName)) {
    // get the proper profile file
    shellProfiles[shellName].forEach((candidate) => {
      let candidatePath = path.join(os.homedir(), candidate);
      // if the profile file exists
      if (fs.existsSync(candidatePath)) {
        profilePath = candidatePath;
        let contents = fs.readFileSync(profilePath, 'utf8');
        let lines = contents.split('\n');
        for (const line of lines) {
          // split each line on the first '='
          const [key, ...rest] = line.split('=');
          // join the rest of the line back together
          const value = rest.join('=');
          // add the key value pair to the env object
          if (key.startsWith('FSLDIR')) {
            fslEnv[key] = value;
          }
        }
        return
      }
    })

  } else if (cShells.includes(shellName)) {
    shellProfiles[shellName].forEach((candidate) => {
      let candidatePath = path.join(os.homedir(), candidate);
      // if the profile file exists
      if (fs.existsSync(candidatePath)) {
        profilePath = candidatePath;
        let contents = fs.readFileSync(profilePath, 'utf8');
        let lines = contents.split('\n');
        for (const line of lines) {
          // if line starts with 'setenv FSLDIR'
          if (line.startsWith('setenv FSLDIR')) {
            // the value of FSLDIR is everything after 'setenv FSLDIR'
            let value = line.replace('setenv FSLDIR ', '');
            // add the key value pair to the env object
            fslEnv['FSLDIR'] = value;
          }
        }
        return
      }
    })
  }
  // add fslEnv keys and values to the process env
  for (const [key, value] of Object.entries(fslEnv)) {
    process.env[key] = value;
  }
  return fslEnv;
}


/**
 * Determines if the application is running in development mode.
 * @returns {boolean} True if the application is running in development mode, false otherwise.
 */
function isDev() {
  // process.argv is an array of the command line arguments
  // the first two are the path to the node executable and the path to the script being run
  // the third is the first argument passed to the app
  // if it's "--dev", we're in development mode
  // return process.argv[2] === '--dev';
  return argv['dev'];
}

/**
 * Registers IPC listeners for the events object.
 */
function registerIpcListeners() {
  for (const [key, value] of Object.entries(events)) {
    if (key === 'launchGui') {
      ipcMain.handle(key, (event, obj) => {
        createWindow(obj.guiName);
      })
    } else {

      /**
       * The handler function for the event.
       * @param {Electron.IpcMainInvokeEvent} event - The event object.
       * @param {object} obj - The object containing the event arguments.
       * @returns {Promise<any>} A promise that resolves to the result of the event handler.
       * @async
       * @function
       * @example
       * ipcRenderer.invoke('fslVersion').then((fslVersion) => {
       *  console.log(fslVersion);
       */
      async function handler(event, obj) {
        const result = await value(obj);
        return result;
      }
      ipcMain.handle(key, handler);
    }
  }
  // create the initial window, start by showing the dashboard gui by default
  let gui = argv['gui'];
  createWindow(gui ? gui : 'dashboard');
}

/**
 * Launches an external GUI process.
 * @param {string} guiName - The name of the GUI to launch.
 */
function launchExternalGui(guiName) {
  const externalGuis = [
    'fsleyes'
  ];

  if (externalGuis.includes(guiName)) {
    const { spawn } = require('child_process');
    const child = spawn(guiName, [], {
      detached: true,
      stdio: 'ignore'
    });
    child.unref();
    return true;
  } else {
    return false;
  }
}

/**
 * Creates a new browser window for the specified GUI.
 * @param {string} guiName - The name of the GUI to create a window for.
 */
function createWindow(guiName = 'dashboard') {

  // set app icon (forcibly)
  app.dock.setIcon(path.join(__dirname, 'icons', 'fsl-desktop.png'));

  if (launchExternalGui(guiName)) {
    return;
  }
  // Create the browser window.
  console.log('creating window')
  console.log(__dirname)
  const mainWindow = new BrowserWindow({
    icon: path.join(__dirname, 'icons', 'fsl-desktop.png'),
    width: 1000,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  if (isDev()) {
    try {
      mainWindow.loadURL(`http://localhost:${devPorts[guiName]}`);
    } catch (err) {
      console.log(`Error loading ${guiName} at http://localhost:${devPorts[guiName]}`);
      console.log(err);
    }
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
  } else {
    const FSLDIR = process.env.FSLDIR;
    // if guiName is dashboard, then the path is relative to the app root
    if (guiName === 'dashboard') {
      mainWindow.loadFile(path.join(__dirname, 'dist', 'index.html'));
      return;
    }
    // check if FSLDIR is undefined
    if (FSLDIR === undefined) {
      console.log('FSLDIR is undefined');
      return;
    }
    // construct path to the requested gui
    // the gui is expected to be in $FSLDIR/share/fsl/gui/${guiName}
    // the gui is expected to have an index.html file
    let guiPath = path.join(FSLDIR, 'share', 'fsl', 'gui', guiName, 'index.html');
    // check if the file at guiPath exists
    let guiPathExists = fs.existsSync(guiPath);
    if (guiPathExists) {
      console.log(`Loading ${guiName} at ${guiPath}`)
      mainWindow.loadFile(guiPath);
    } else {
      console.log(`Error loading ${guiName} at ${guiPath}`)
    }

  }
};

function onReady() {
  registerIpcListeners();
  const fslEnv = getFslEnv();
  setMenu();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', onReady);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  // close the file server gracefully
  fileServer.kill()
  app.quit();
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
  }
});

function setMenu() {
  // create an application menu with an "apps" submenu that lists all the available FSL apps
  // FSL apps are: bet, flirt, fast, fslstats
  // each app should have a keyboard shortcut
  let menu = [
    {
      label: 'Tools',
      submenu: [
        // {
        //   label: 'bet',
        //   accelerator: 'CmdOrCtrl+B',
        //   click: () => {
        //     createWindow('bet');
        //   }
        // },

        // add all the installed guis to the menu
        ...listGuisSync().map((gui) => {
          return {
            label: gui.name,
            click: () => {
              createWindow(gui.name);
            }
          }
        }),

        // add a separator
        { type: 'separator' },
        // dashboard gui is always available because it is bundled with the desktop app
        {
          label: 'dashboard',
          click: () => {
            createWindow('dashboard');
          }
        },

      ]
    },
    // add window menu with reload options
    {
      label: 'Window',
      submenu: [
        {
          label: 'Reload',
          accelerator: 'CmdOrCtrl+R',
          click: () => {
            BrowserWindow.getFocusedWindow().reload();
          }
        },
        {
          label: 'Toggle DevTools',
          accelerator: 'CmdOrCtrl+Shift+I',
          click: () => {
            BrowserWindow.getFocusedWindow().toggleDevTools();
          }
        }
      ]
    }
  ];
  // Add macOS application menus
  if (process.platform === 'darwin') {
    menu.unshift({
      label: app.name,
      submenu: [
        { role: 'about' },
        { type: 'separator' },
        { role: 'services', submenu: [] },
        { type: 'separator' },
        { role: 'hide' },
        { role: 'hideothers' },
        { role: 'unhide' },
        { type: 'separator' },
        { role: 'quit' }
      ]
    });
  }

  Menu.setApplicationMenu(Menu.buildFromTemplate(menu));
}

